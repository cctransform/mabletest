resource "aws_route_table" "proxy" {
  vpc_id = "${aws_vpc.mable-test.id}"

  tags {
    environment         = "${var.environment_name}"
    division            = "mable-test"
    service_line        = "EREF"
    data_classification = "unclass"
    product             = "NULL"
    Name                = "VPC-CD-${var.environment_name}-Proxy"
  }
}

resource "aws_route_table_association" "proxy" {
  count          = "2"
  subnet_id      = "${aws_subnet.proxy.*.id[count.index]}"
  route_table_id = "${aws_route_table.proxy.id}"
}

# XXX: do we need an IGW?
#data "aws_internet_gateway" "default" {
#  internet_gateway_id = "${var.igw_id}"
#}



#resource "aws_route" "internet" {
#  route_table_id         = "${aws_route_table.proxy.id}"
#  destination_cidr_block = "0.0.0.0/0"
#  gateway_id             = "${data.aws_internet_gateway.default.id}"
#}
