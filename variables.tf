// Module specific variables

// The terraform environment specifies the HDA environment
// More info: https://www.terraform.io/docs/state/environments.html

//
// Variables defined in src/envvars/${env}.tf
//
#This variable is for defining ElasticSearch hosts
#Datadog config
variable "hostname" {}



variable "environment_name" {
  type        = "string"
}

#variable "vpc_id" {}

# CIDR ranges for the proxy subnet
variable "proxy_subnet_cidrs" {
  type = "list"
}


variable "main_subnet_cidrs" {
  type = "list"
}


#variable "alb_subnets" {
 # type = "list"
#}

#variable "asg_subnets" {
#  type = "list"
#}

variable "RAILS_ROOT" {
  type = "string"
  default = "/var/www/app_name"
}

#variable "vpc_default_asg_group" {}
variable "asg_desired_capacity" {}

// Doesn't currently change
variable "asg_azs" {
  default = ["ap-southeast-2a", "ap-southeast-2b"]
}

variable "user_data_path" {
  default = "user-data.sh.tpl"
}

variable "asg_office_hours_scheduled_scaling" {
  // Set to "true" to make the stack's instances only run during 7am-6pm (or so)
  // as a money saving mechanism.
  default = "false"
}

variable "ereferral_dns_name_prefix" {
  type = "map"

  default = {
    dev = "dev."
    prd = ""
  }
}



variable "asg_instance_type" {}

variable "asg_min_capacity" {
  #  default = 2
}
