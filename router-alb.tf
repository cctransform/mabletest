resource "aws_alb" "router" {
  name            = "mablerou-${var.environment_name}"
  internal        = true
  security_groups = ["${aws_security_group.basic.id}", "${aws_security_group.router-alb.id}"]
  subnets         = ["${aws_subnet.main.*.id}"]

  tags {
    environment         = "${var.environment_name}"
    division            = "mable"
    service_line        = "mable"
    data_classification = "unclass"
    product             = "NULL"
    Name                = "mablerou-${var.environment_name}"
  }
}

resource "aws_alb_target_group" "router" {
  name_prefix          = "mable"
  port                 = "443"
  protocol             = "HTTPS"
  vpc_id               = "${aws_vpc.mable-test.id}"
  deregistration_delay = "30"

  health_check {
    interval = "10"
    timeout  = "3"
    path     = "/"
    port     = "traffic-port"
    protocol = "HTTPS"
    matcher  = "200"
  }

  tags {
    environment         = "${var.environment_name}"
    division            = "mable"
    service_line        = "mable"
    data_classification = "unclass"
    product             = "NULL"
    Name                = "mablerou-${var.environment_name}"
  }
}

resource "aws_alb_listener" "router-https" {
  load_balancer_arn = "${aws_alb.router.id}"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = "${data.aws_acm_certificate.alb.arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.router.arn}"
    type             = "forward"
  }
}

// ALB
resource "aws_security_group" "router-alb" {
  name        = "mable-router-${var.environment_name}-alb"
  description = "Allow inbound traffic on tcp/443 from anywhere"
  vpc_id      = "${aws_vpc.mable-test.id}"

  tags {
    environment         = "${var.environment_name}"
    division            = "mable"
    service_line        = "mable"
    data_classification = "unclass"
    product             = "NULL"
    Name                = "mable-router-${var.environment_name}-alb"
  }
}

resource "aws_security_group_rule" "alb_ingress_https_all" {
  type      = "ingress"
  from_port = 443
  to_port   = 443
  protocol  = "tcp"

  # TODO: replace cidr_blocks with source_security_group_id once Nemesis tell us which one to use
  cidr_blocks = ["0.0.0.0/0"]

  # source_security_group_id = "${aws_security_group.asg.id}"
  security_group_id = "${aws_security_group.router-alb.id}"
}

resource "aws_security_group_rule" "alb_egress_https" {
  type                     = "egress"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.router-asg.id}"
  security_group_id        = "${aws_security_group.router-alb.id}"
}

data "aws_acm_certificate" "alb" {
  domain   = "*.sydneyvinayak.org"
  statuses = ["ISSUED"]
}

resource "aws_route53_record" "router" {
  zone_id = "${data.aws_route53_zone.mable.zone_id}"
  name    = "router.${var.environment_name}.mable"
  type    = "A"

  alias {
    name                   = "${aws_alb.router.dns_name}"
    zone_id                = "${aws_alb.router.zone_id}"
    evaluate_target_health = false
  }
}
