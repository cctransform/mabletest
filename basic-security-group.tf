// Simple SG to allow basic connectivity
// Assumes egress is unrestricted
resource "aws_security_group" "basic" {
  name        = "mable-${var.environment_name}-basic"
  description = "Basic connectivity permissions"
  vpc_id      = "${aws_vpc.mable-test.id}"

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  // DNS
  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    environment         = "${var.environment_name}"
    division            = "mable"
    service_line        = "mable"
    data_classification = "unclass"
    product             = "NULL"
    Name                = "mable-${var.environment_name}-basic"
  }
}
