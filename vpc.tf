resource "aws_vpc" "mable-test" {
  cidr_block       = "10.20.0.0/18"

  tags = {
    Name = "mable-test"
    Organisation  = "ACM"
    Terraform     = "true"
    managed_by    = "Terraform"
  }
}


# Create Management route table
resource "aws_route_table" "mgmt-routetable" {
    vpc_id = "${aws_vpc.mable-test.id}"
    tags {
        Name = "mgmt-routetable"
    }
}


# Create VPC Internet Gateway
resource "aws_internet_gateway" "proxy-igw" {
    vpc_id = "${aws_vpc.mable-test.id}"
    tags {
        Name = "proxy-igw"
    }
}

/* */
# Create default route for Management route table
resource "aws_route" "mgmt-default-route" {
    route_table_id = "${aws_route_table.mgmt-routetable.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.proxy-igw.id}"
    depends_on = [
        "aws_route_table.mgmt-routetable",
        "aws_internet_gateway.proxy-igw"
    ]
}



resource "aws_route_table_association" "public" {
  count          = "2"
  subnet_id      = "${aws_subnet.main.*.id[count.index]}"
  route_table_id = "${aws_route_table.mgmt-routetable.id}"
}



# Create default VPC Network ACL
resource "aws_network_acl" "default-network-acl" {
    vpc_id = "${aws_vpc.mable-test.id}"
    egress {
        protocol = "-1"
        rule_no = 100
        action = "allow"
        cidr_block = "0.0.0.0/0"
        from_port = 0
        to_port = 0
    }
    ingress {
        protocol = "-1"
        rule_no = 100
        action = "allow"
        cidr_block = "0.0.0.0/0"
        from_port = 0
        to_port = 0
    }
    subnet_ids = [
        "${aws_subnet.proxy.*.id}"
    ]
    tags {
        Name = "default ACL"
    }
}

# Create default VPC security group
resource "aws_security_group" "default-security-gp" {
    name = "proxy-allow-all"
    vpc_id = "${aws_vpc.mable-test.id}"
    description = "Allow all inbound traffic"
    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags {
        Name = "proxy-allow-all"
    }
}


resource "aws_subnet" "main" {
  count             = "${length(var.main_subnet_cidrs)}"
  vpc_id     = "${aws_vpc.mable-test.id}"
  cidr_block        = "${var.main_subnet_cidrs[count.index]}"
  availability_zone = "${var.asg_azs[count.index]}"

  tags = {
    Name = "Main"
  }
}
