// Provider specific configs
provider "aws" {
  region  = "ap-southeast-2"
  version = "~> 1.7"


}

provider "template" {}

provider "terraform" {}

#data "aws_iam_account_alias" "current" {}

data "aws_caller_identity" "current" {}


// Write state to s3
terraform {
  backend "s3" {
    #bucket = "mable-test"
    #key    = "terraform/mable-test/terraform.tfstate"
    #region = "ap-southeast-2"
    # dynamodb_table = "mable-test"
  }
}

// Load DNS zone
data "aws_route53_zone" "mable" {
  name = "sydneyvinayak.org."
}

// Load AMI
data "aws_ami" "asg_ec2_ami" {
  most_recent = true

  filter {
    name   = "tag:Name"
    values = ["mabletestawslinux2"]
  }
}
