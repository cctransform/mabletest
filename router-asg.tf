resource "aws_autoscaling_group" "router" {
  name = "mablerou-${var.environment_name}r - ${aws_launch_configuration.router.name}"

  availability_zones        = "${var.asg_azs}"
  max_size                  = 5
  min_size                  = "${var.asg_min_capacity}"
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = "${var.asg_desired_capacity}"

  launch_configuration = "${aws_launch_configuration.router.name}"
  target_group_arns    = ["${aws_alb_target_group.router.arn}"]
  min_elb_capacity     = 1
  vpc_zone_identifier  = ["${aws_subnet.proxy.*.id}"]

  enabled_metrics = ["GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]

  tags = [
    {
      key                 = "environment"
      value               = "${var.environment_name}"
      propagate_at_launch = true
    },
    {
      key                 = "division"
      value               = "mable"
      propagate_at_launch = true
    },
    {
      key                 = "service_line"
      value               = "mable"
      propagate_at_launch = true
    },
    {
      key                 = "data_classification"
      value               = "unclass"
      propagate_at_launch = true
    },
    {
      key                 = "product"
      value               = "NULL"
      propagate_at_launch = true
    },
    {
      key                 = "Name"
      value               = "mablerou-${var.environment_name}r"
      propagate_at_launch = true
    },
  ]

  # See https://www.terraform.io/docs/providers/aws/r/launch_configuration.html#using-with-autoscaling-groups
  lifecycle {
    create_before_destroy = false
  }
}

resource "aws_launch_configuration" "router" {
  name_prefix          = "mablerou-${var.environment_name}r"
  image_id             = "${data.aws_ami.asg_ec2_ami.image_id}"
  instance_type        = "${var.asg_instance_type}"
  security_groups      = ["${aws_security_group.default-security-gp.id}", "${aws_security_group.basic.id}", "${aws_security_group.router-asg.id}"]
  user_data            = "${data.template_file.user_data.rendered}"
  key_name             = "mable-test"
  iam_instance_profile = "${aws_iam_instance_profile.router.arn}"

  # See https://www.terraform.io/docs/providers/aws/r/launch_configuration.html#using-with-autoscaling-groups
  lifecycle {
    create_before_destroy = false
  }
}

resource "aws_security_group" "router-asg" {
  name        = "mable-${var.environment_name}-router-asg"
  description = "Allow inbound traffic from router ALB to HTTPS"
  vpc_id      = "${aws_vpc.mable-test.id}"

  tags {
    environment         = "${var.environment_name}"
    division            = "mable"
    service_line        = "mable"
    data_classification = "unclass"
    product             = "NULL"
    Name                = "mable-${var.environment_name}-router-asg"
  }
}

resource "aws_security_group_rule" "router-asg_ingress_https" {
  type                     = "ingress"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.router-alb.id}"
  security_group_id        = "${aws_security_group.router-asg.id}"
}

resource "aws_security_group_rule" "router-asg_ingress_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.router-asg.id}"
}

resource "aws_security_group_rule" "router-asg_egress_allow_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.router-asg.id}"
}

resource "aws_autoscaling_schedule" "router-scaledown" {
  count                 = "${var.asg_office_hours_scheduled_scaling == "true" ? 1 : 0}"
  scheduled_action_name = "out of hours shutdown"
  min_size              = 0
  max_size              = 1
  desired_capacity      = 0

  # m h dom mon dow
  # 11am = 9pm AEST
  recurrence = "0 11 * * 1-5"

  autoscaling_group_name = "${aws_autoscaling_group.router.name}"
}

resource "aws_autoscaling_schedule" "router-scaleup" {
  count                 = "${var.asg_office_hours_scheduled_scaling == "true" ? 1 : 0}"
  scheduled_action_name = "out of hours startup"
  min_size              = 1
  max_size              = 5
  desired_capacity      = "${var.asg_desired_capacity}"

  # m h dom mon dow
  # 7am AEST
  recurrence = "0 21 * * 1-5"

  autoscaling_group_name = "${aws_autoscaling_group.router.name}"
}

# scale up alarm
resource "aws_autoscaling_policy" "mablerouter-scaleup-cpu" {
  name                   = "mablerouter-${var.environment_name}-cpu-policy-scaleup"
  autoscaling_group_name = "${aws_autoscaling_group.router.name}"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "High-cpu-alarm" {
  alarm_name          = "mablerouter-${var.environment_name}-high-cpu-alarm"
  alarm_description   = "mablerouter-alarm-high-CPU-ScaleUp"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Maximum"
  threshold           = "70"

  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.router.name}"
  }

  actions_enabled = true
  alarm_actions   = ["${aws_autoscaling_policy.mablerouter-scaleup-cpu.arn}"]
}

# scale down alarm
resource "aws_autoscaling_policy" "mablerouter-scaledown-cpu" {
  name                   = "mablerouter-${var.environment_name}-cpu-policy-scaledown"
  autoscaling_group_name = "${aws_autoscaling_group.router.name}"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "-1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "Low-cpu-alarm" {
  alarm_name          = "mablerouter-${var.environment_name}-low-cpu-alarm"
  alarm_description   = "mablerouter-alarm-low-CPU-ScaleDown"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "50"

  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.router.name}"
  }

  actions_enabled = true
  ok_actions      = ["${aws_autoscaling_policy.mablerouter-scaledown-cpu.arn}"]
}

data "template_file" "user_data" {
  template = "${file("${var.user_data_path}")}"

  vars {
    hostname                  = "${var.hostname}"
    RAILS_ROOT = "${var.RAILS_ROOT}"
    environment_name                     = "${var.environment_name}"
  }
}
