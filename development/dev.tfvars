hostname = "mablerou-devr"

environment_name = "development"

#environment_name = "nonprod"

#vpc_id = "vpc-1c515a7e"

#alb_subnets = ["subnet-dd210ba9", "subnet-cb525aa9"]

#asg_subnets = ["subnet-33322e45", "subnet-09b6596e"]

#vpc_default_asg_group = "sg-d4beb4b6"

proxy_subnet_cidrs = ["10.20.4.0/22", "10.20.8.0/22"]

main_subnet_cidrs = ["10.20.16.0/22", "10.20.12.0/22"]
proxy_subnets_overall_cidr = "10.20.0.0/18"

asg_desired_capacity = "1"

asg_min_capacity = "1"

#resolver_ips = ["10.51.13.10", "10.51.14.10"]

asg_office_hours_scheduled_scaling = "false"

asg_instance_type = "t2.micro"
