resource "aws_subnet" "proxy" {
  count             = "${length(var.proxy_subnet_cidrs)}"
  vpc_id            = "${aws_vpc.mable-test.id}"
  cidr_block        = "${var.proxy_subnet_cidrs[count.index]}"
  availability_zone = "${var.asg_azs[count.index]}"

  tags {
    environment         = "${var.environment_name}"
    division            = "mable-test"
    service_line        = "EREF"
    data_classification = "unclass"
    product             = "NULL"
    Name                = "ProxyAZ${count.index+1}-${var.environment_name}"
  }
}
