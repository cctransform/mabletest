#!/bin/bash -eux

date '+%Y-%m-%d %H:%M:%S'
echo '===> Running user-data script'

# These variables come from terraform
RAILS_ROOT="${RAILS_ROOT}"

echo '===> Installing python-pip'
yum -y install python-pip

echo '===> Installing awscli'
pip install awscli



########
# Set up nginx
########
# Disable SELinux since it prevents nginx sending requests to slr:8080
# TODO: should be able to just enable the port following these instructions:
# https://serverfault.com/questions/566317/nginx-no-permission-to-bind-port-8090-but-it-binds-to-80-and-8080
echo "===> Disabling SELinux"
setenforce 0

#ssl_hostname="router.$environment_name.mable.sydneyvinayak.org.au"
#echo "===> SSL hostname is $ssl_hostname"

echo "===> Customising limits.conf for nginx"
cat >>/etc/security.limits.conf <<EOF
nginx       soft    nofile   10000
nginx       hard    nofile   10000
EOF

echo '===> Adding nginx yum repo'
cat <<EOF >/etc/yum.repos.d/nginx.repo
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/mainline/rhel/7/x86_64/
gpgcheck=0
enabled=1
EOF

echo '===> Installing nginx'
yum -y install nginx

echo '===> Generating self-signed certificate'
openssl req -x509 -sha256 \
  -newkey rsa:2048 -keyout /etc/nginx/key.pem \
  -out /etc/nginx/cert.pem \
  -days 365 -nodes \
  -subj "/CN=sydneyvinayak.org"
chmod 400 /etc/nginx/key.pem /etc/nginx/cert.pem

echo '===> Customising nginx config'
rm /etc/nginx/conf.d/default.conf

# Disable the default access log -- we set up our own log format and use the same file
sed -i'' -e '/access_log/d' /etc/nginx/nginx.conf

# Set max open files based on settings in /etc/security/limits.conf
echo 'worker_rlimit_nofile 10000;' >> /etc/nginx/nginx.conf

# Remember to escape every variable in this cat call!
cat <<EOF >/etc/nginx/conf.d/mable-router.conf
upstream rails_app {
   server app:3000;
}
server {
   # define your domain  
   server_name localhost;   
   # define the public application root  
   root   $RAILS_ROOT/public;  
   index  index.html;
   # define where Nginx should write its logs  
   access_log $RAILS_ROOT/log/nginx.access.log;  
   error_log $RAILS_ROOT/log/nginx.error.log;   
  
   # deny requests for files that should never be accessed  
   location ~ /\. {    
      deny all;  
   }
   location ~* ^.+\.(rb|log)$ {    
      deny all;  
   }  
 
    # serve static (compiled) assets directly if they exist (for rails production)  
    location ~ ^/(assets|images|javascripts|stylesheets|swfs|system)/   {    
      try_files $uri @rails;     
      access_log off;    
      gzip_static on; 
      # to serve pre-gzipped version     
      expires max;    
      add_header Cache-Control public;     
      
      add_header Last-Modified "";    
      add_header ETag "";    
      break;  
   } 
    location / {
        return 200;
    }
   # send non-static file requests to the app server  
    location / {    
      try_files $uri @rails;  
    }   
    location @rails {    
      proxy_set_header  X-Real-IP  $remote_addr;    
      proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;         
      proxy_set_header Host $http_host;    
      proxy_redirect off;    
      proxy_pass http://rails_app;  
    }
}
EOF

echo '===> Starting nginx'
systemctl start nginx
