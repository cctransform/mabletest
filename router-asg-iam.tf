resource "aws_iam_role" "router" {
  name               = "mable-router-asg-${var.environment_name}"
  assume_role_policy = "${data.aws_iam_policy_document.instance-assume-role-policy.json}"
}


resource "aws_iam_policy" "router" {
  name        = "mable-router-asg-${var.environment_name}"
  description = "mablerou ASG policy -- allow access to sitemap files in s3"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject",
        "s3:ListBucket"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "router" {
  role       = "${aws_iam_role.router.name}"
  policy_arn = "${aws_iam_policy.router.arn}"
}

resource "aws_iam_instance_profile" "router" {
  name = "mable-router-asg-${var.environment_name}"
  role = "${aws_iam_role.router.name}"
}
